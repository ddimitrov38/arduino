#include <avr/io.h>
#include <util/delay.h>

#define DELAY 1000

int main (void) {
	DDRB |= 0b00100000;

	while(1) {
		PORTB |= _BV(PORTB5);
		_delay_ms(DELAY);
		PORTB &= ~_BV(PORTB5);
		_delay_ms(DELAY);
	}

	return 0;
}

