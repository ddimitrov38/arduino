// Embeded morse code encoder
// Dimitar T. Dimitrov 
// Sofia, Bulgaria 2016

#include <avr/io.h>
#include <util/delay.h>

const char morsetable[] = "!HJ4\x10\x42\x36@ G5D#\"7FM20\x11\x31\x41\x33IKL"; // Packed (size{4}-dots/dashes{4}): eg. A(.-): 00100001 => ascii "!"

#define DELAY 1000
#define BLINK_DELAY 300
#define DOT_DELAY 150
#define DASH_DELAY 500

void blink(char dot);


char uppercase(char c) { return ((c >= 0x61) && (c <= 0x7A))?c-0x20:c; }

void morsencode(char letter) {
	int i = 0;
	int idx = uppercase(letter) - 65;

	if (idx < 0 || idx > 26) return;

	for (i = (morsetable[idx] >> 4) - 1; i >= 0; i--)
		blink( (((morsetable[idx] & 0xf) & ( 1 << i )) >> i) == 1?'-':'.');
}

void blink(char code) {
	PORTB |= _BV(PORTB5); // ON
	if (code == '.')
		_delay_ms(DOT_DELAY);
	else 
		_delay_ms(DASH_DELAY);

	PORTB &= ~_BV(PORTB5);  // OFF
	_delay_ms(BLINK_DELAY);
}


int main (void) {
	char *test = "SOS!";
	char *p = test;

	DDRB |= 0b00100000;

	while (42) {
		while (*p)
			morsencode(*p++);

		p = test; // Reset pointer
		_delay_ms(DELAY);
	}

	return 0;
}
