// Embeded morse code encoder
// Dimitar T. Dimitrov 
// Sofia, Bulgaria 2016

#include <stdlib.h>
#include <stdio.h>

const char morsetable[] = "!HJ4\x10\x42\x36@ G5D#\"7FM20\x11\x31\x41\x33IKL"; // Packed (size{4}-dots/dashes{4}): eg. A(.-): 00100001 => ascii "!"

char uppercase(char c) { return ((c >= 0x61) && (c <= 0x7A))?c-0x20:c; }

void morsencode(char letter) {
	int idx = uppercase(letter) - 65;

	if (idx < 0 || idx > 26) {
		printf("%c", letter);
		return;
	}

	for (int i = (morsetable[idx] >> 4) - 1; i >= 0; i--)
		printf("%c", (((morsetable[idx] & 0xf) & ( 1 << i )) >> i) == 1?'-':'.');

}

int main(int ac, char **av) {
	char *test = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	while (*test) {
		morsencode(*test++);
		printf(" ");
	}
	printf("\n");

	return 0;
}
